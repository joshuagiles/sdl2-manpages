# Manpage generator for SDL2
Simple Doxygen configuration and Makefile for building and optionally
installing manpages generated from SDL headers.

## Simple usage
Ensure you have SDL2 and Doxygen installed. Then run

    make # Builds the manpages; see `./build/man`.
    make install # Install the manpages to `/usr/local/man` (may require sudo).
